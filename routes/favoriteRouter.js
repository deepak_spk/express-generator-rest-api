const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');
const Favorites = require('../models/favorite');
const cors = require('./cors');

const favoriteRouter = express.Router();

favoriteRouter.use(bodyParser.json());

favoriteRouter.route('/')
	.options(cors.corsWithOptions, authenticate.verifyUser, (req, res) => { res.sendStatus(200); })
	.get(cors.cors, authenticate.verifyUser, (req, res, next) => {
		Favorites.find({user: req.user._id})
        .populate('user')
        .populate('dishes')
			.then((favorites) => {
				res.statusCode = 200;
				res.setHeader('Content-Type', 'application/json');
				res.json(favorites);
			}, (err) => next(err))
			.catch((err) => next(err));
	})
	.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {

        //if no dishes are provided
        if (!req.body || !req.body.length) {
            next(new Error('No dishes are provided!'));
        }

        Favorites.find({user: req.user._id})
			.then((favorites) => {
                //There no any favorites for the user in the db yet.
                //Create new.
				if (!favorites.length) {
                    //Build favorite revord.
                    let favorite = {user: req.user._id};
                    let dishes = [];
                    req.body.forEach((dish) => {
                        dishes.push(dish._id);
                    });
                    favorite.dishes = dishes;
                    
                    Favorites.create(favorite)
                        .then((favorite) => {
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json');
                            res.json(favorite);
                        }, (err) => next(err))
                        .catch((err) => next(err));
                } else {

                    let favorite = favorites[0];

                    //Go throughout the provided dishes' ids and if they were not persisted as favorites dishes early - add them.
                    req.body.forEach((dish) => {
                        if (favorite.dishes.indexOf(dish._id) === -1) {
                            favorite.dishes.push(dish._id);
                        }
                    });
                    favorite.save()
						.then((favorite) => {
							res.statusCode = 200;
							res.setHeader('Content-Type', 'application/json');
							res.json(favorite);
						}, (err) => next(err));
                }
			}, (err) => next(err))
			.catch((err) => next(err));
		
	})
	.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
		res.statusCode = 403;
		res.end('PUT operation not supported on /favorites');
	})
	.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
		Favorites.remove({user: req.user._id})
			.then((resp) => {
				res.statusCode = 200;
				res.setHeader('Content-Type', 'application/json');
				res.json(resp);
			}, (err) => next(err))
			.catch((err) => next(err));
	});

favoriteRouter.route('/:dishId')
	.options(cors.corsWithOptions, authenticate.verifyUser, (req, res) => { res.sendStatus(200); })
	.get(cors.cors, authenticate.verifyUser, (req, res, next) => {
		res.statusCode = 403;
		res.end(`GET operation not supported on /favorites/${req.params.dishId}`);
	})
	.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {

        Favorites.find({user: req.user._id})
			.then((favorites) => {
                //There no any favorites for the user in the db yet.
                //Create new.
				if (!favorites.length) {
                    //Build favorite revord.
                    let favorite = {user: req.user._id};
                    favorite.dishes = [req.params.dishId];
                    
                    Favorites.create(favorite)
                        .then((favorite) => {
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json');
                            res.json(favorite);
                        }, (err) => next(err))
                        .catch((err) => next(err));
                } else {
                    let favorite = favorites[0];

                    //Check if the dish was persisted as the favorite one early.
                    if (favorite.dishes.indexOf(req.params.dishId) === -1) {
                        favorite.dishes.push(req.params.dishId);
                        favorite.save()
                            .then((favorite) => {
                                res.statusCode = 200;
                                res.setHeader('Content-Type', 'application/json');
                                res.json(favorite);
                            }, (err) => next(err));
                    
                    //If already favorite - just provide the success response.
                    } else {
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(favorite);
                    }
                    
                }
			}, (err) => next(err))
			.catch((err) => next(err));
		
	})
	.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
		res.statusCode = 403;
		res.end(`PUT operation not supported on /favorites/${req.params.dishId}`);
	})
	.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
		Favorites.find({user: req.user._id})
			.then((favorites) => {

                //Check that the dish is persisted as the favorite one.
                if (favorites.length && favorites[0].dishes.indexOf(req.params.dishId) !== -1) {

                    let favorite = favorites[0];

                    //Remove from the array of favorite dishes.
                    let index = favorite.dishes.indexOf(req.params.dishId);
                    favorite.dishes.splice(index, 1);

                    //If there are still some favorites.
                    if (favorite.dishes.length) {
                        favorite.save()
                            .then((favorite) => {
                                res.statusCode = 200;
                                res.setHeader('Content-Type', 'application/json');
                                res.json(favorite);
                            }, (err) => next(err));
                    //Otherwise.
                    } else {
                        Favorites.remove({user: req.user._id})
                            .then((resp) => {
                                res.statusCode = 200;
                                res.setHeader('Content-Type', 'application/json');
                                res.json(resp);
                            }, (err) => next(err))
                            .catch((err) => next(err));
                    }

                //Otherwise just provide the success response.
                } else {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorite);
                }
			}, (err) => next(err))
			.catch((err) => next(err));
    });
    
module.exports = favoriteRouter;